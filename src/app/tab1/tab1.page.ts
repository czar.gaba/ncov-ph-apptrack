import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestService } from '../rest.service';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  public datax : any = [];
  public data1 : any = [];
  public data2 : any = [];
  public data3 : any = [];
  public datacount : any = [];
  public death1 : any = [];
  public recover1 : any = [];
  public admitted : any = [];
  
  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router ,private splashScreen: SplashScreen) { }
  ngOnInit() {
    this.splashScreen.show();
    this.splashScreen.hide();
  this.rest.getDataxx("test-results").subscribe((data: {}) => {
    console.log(data);
    this.datax = data;
  });

  let sum = 0;
  let death1 = 0;
  let recover=0;
  let admitted=0;
  this.rest.getDataxx("cases").subscribe((data: {}) => {
    // console.log(data);
    this.data3 = data;
    
    
    // console.log(this.data3.length);
    this.datacount = this.data3.length;

    
    const stats = "Died";
    const countd = this.data3.reduce((acc, cur) => cur.status === stats ? ++acc : acc, 0);

    // console.log("stats   "+countd);
    this.death1 = countd;

    const recov = "Recovered";
    const countr = this.data3.reduce((acc, cur) => cur.status === recov ? ++acc : acc, 0);

    // console.log("stats   "+countr);
    this.recover1 = countr;


    const  admittedx = "Admitted";
    const counta = this.data3.reduce((acc, cur) => cur.status === admittedx ? ++acc : acc, 0);

    // console.log("stats   "+counta);
    this.admitted = counta;

  });





  this.rest.getDataxx("patients-under-investigation").subscribe((datad: {}) => {
    // console.log(datad);
    this.data1 = datad;


  });


 
  
  }

  
  doRefresh(event) {
    

    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

}
